from odoo import fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    instructor = fields.Boolean('Instructor', default=False)

    session_ids = fields.Many2many('ta.session', string='Sesi yang dihadiri',
                                   readonly=True)
