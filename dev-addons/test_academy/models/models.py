# -*- coding: utf-8 -*-

from datetime import timedelta
from odoo import models, fields, api, exceptions


class Course(models.Model):
    _name = 'ta.course'

    name = fields.Char(string='Judul', required=True)
    description = fields.Text()
    state = fields.Selection(selection=[('draft', 'draft'),
                                        ('confirmed', 'confirmed'),
                                        ('done', 'done')])
    responsible_id = fields.Many2one('res.users', ondelete='set null',
                                     string='Penanggung jawab', index=True)
    session_ids = fields.One2many('ta.session', 'course_id', string='Daftar Sesi')

    _sql_constraints = [
        ('name_description_check',
         'CHECK(name != description)',
         'Judul tidak boleh sama dengan deskripsi, males tenan ik'),
        ('name_unique',
         'UNIQUE(name)',
         'Nama mata kuliah kok podo, marakke bingung peserta tho'),
    ]

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        copied_count = self.search_count(
            [('name', '=like', u'Copy of {}'.format(self.name))]
        )
        if not copied_count:
            new_name = u'Copy of {}'.format(self.name)
        else:
            new_name = u'Copy of {} ({})'.format(self.name, copied_count)

        default['name'] = new_name
        return super(Course, self).copy(default)


class Session(models.Model):
    _name = 'ta.session'

    name = fields.Char(required=True)
    start_date = fields.Date(default=fields.Date.today)
    duration = fields.Float(digits=(6,2), help="durasi dalam hari")
    seats = fields.Integer(string='Jumlah tempat duduk')
    active = fields.Boolean(default=True)
    color = fields.Integer()
    instructor_id = fields.Many2one('res.partner', string='Instruktur',
                                    domain=['|', ('instructor', '=', True),
                                            ('category_id', 'ilike', 'Instruktur')])
    course_id = fields.Many2one('ta.course', ondelete='cascade',
                                string='Mata Kuliah', required=True)
    attendee_ids = fields.Many2many('res.partner', string='Peserta')
    taken_seats = fields.Float(string='Kursi terisi', compute='_taken_seats')
    end_date = fields.Date(string='Tanggal Selesai', store=True,
                           compute='_get_end_date', inverse='_set_end_date')
    hours = fields.Float(string='Durasi dalam jam', compute='_get_hours',
                         inverse='_set_hours')
    attendees_count = fields.Integer(string='Jumlah peserta', compute='_get_attendees_count',
                                     store=True)

    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        for rec in self:
            rec.attendees_count = len(rec.attendee_ids)

    @api.depends('duration')
    def _get_hours(self):
        for rec in self:
            rec.hours = rec.duration * 24

    def _set_hours(self):
        for rec in self:
            rec.duration = rec.hours / 24

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for rec in self:
            if not (rec.start_date and rec.duration):
                rec.end_date = rec.start_date
                continue

            start_date = fields.Datetime.from_string(rec.start_date)
            duration = timedelta(days=rec.duration, seconds=-1)
            rec.end_date = start_date + duration

    def _set_end_date(self):
        for rec in self:
            if not (rec.start_date and rec.end_date):
                continue

            start_date = fields.Datetime.from_string(rec.start_date)
            end_date = fields.Datetime.from_string(rec.end_date)
            rec.duration = (end_date - start_date).days + 1


    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for rec in self:
            if not rec.seats:
                rec.taken_seats = 0.0
            else:
                rec.taken_seats = 100.0 * len(rec.attendee_ids) / rec.seats

    @api.onchange('seats', 'attendee_ids')
    def _verify_valid_seats(self):
        if self.seats < 0:
            return {
                'warning': {
                    'title': 'Nilai tempat duduk negatif',
                    'message': 'Jumlah tempat duduk tidak boleh negatif, harus selalu positif',
                }
            }
        if self.seats < len(self.attendee_ids):
            return {
                'warning': {
                    'title': 'Pendaftar kebanyakan',
                    'message': 'Sudah banyak pendaftar, tolong jangan diubah-ubah tho, kacau tenan',
                }
            }

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for rec in self:
            if rec.instructor_id and rec.instructor_id in rec.attendee_ids:
                raise exceptions.ValidationError('Kalau instruktur ya nggak boleh ikut jadi peserta, ngawur')

