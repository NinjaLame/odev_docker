from odoo import models, fields, api


class Wizard(models.TransientModel):
    _name = 'ta.wizard'

    def _default_session(self):
        return self.env['ta.session'].browse(self._context.get('active_id'))  #

    session_id = fields.Many2one('ta.session', string='Sesi Pilihan', required=True,
                                  default=_default_session)
    attendee_ids = fields.Many2many('res.partner', string='Peserta')

    @api.multi
    def subscribe(self):
        for session in self.session_ids:
            session.attendee_ids |= self.attendee_ids
        return {}
